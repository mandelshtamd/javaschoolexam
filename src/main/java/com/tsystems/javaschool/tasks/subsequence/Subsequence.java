package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();

        if (x.size() > y.size())
            return false;

        // true, because we can just delete all from y
        if (x.isEmpty())
            return true;

        int currFirstSeqElemIndex = 0;
        int currSecondSeqElemIndex = 0;

        // iterate through second list and check if we have subsequence
        while (currSecondSeqElemIndex < y.size()) {
            if (currFirstSeqElemIndex == x.size()) {
                return true;
            }

            if (y.get(currSecondSeqElemIndex) == x.get(currFirstSeqElemIndex)) {
                currFirstSeqElemIndex++;
            }
            currSecondSeqElemIndex++;
        }

        return currFirstSeqElemIndex == x.size();
    }
}
