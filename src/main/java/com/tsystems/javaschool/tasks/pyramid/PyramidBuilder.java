package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            if (inputNumbers.contains(null)) {
                throw new CannotBuildPyramidException();
            }

            Collections.sort(inputNumbers);

            int pyramidHeight = getPyramidHeight(inputNumbers.size());
            int pyramidWidth = 2 * pyramidHeight - 1;
            int[][] pyramid = new int[pyramidHeight][pyramidWidth];

            fillPyramidWithElements(pyramid, inputNumbers);
            return pyramid;

        } catch (Error e) {
            throw new CannotBuildPyramidException();
        }
    }

    private int getPyramidHeight(int length) {
        int elementsCount = 0;
        int heightLevelCounter = 0;

        while (elementsCount < length) {
            heightLevelCounter++;
            elementsCount = elementsCount + heightLevelCounter;
            if (elementsCount > length) {
                throw new CannotBuildPyramidException();
            }
        }
        return heightLevelCounter;
    }

    private void fillPyramidWithElements(int[][] pyramid, List<Integer> elements) {
        int elementsCurrentIndex = 0;
        int startPosition = (pyramid[0].length) / 2;

        for (int heightLevel = 0; heightLevel < pyramid.length; heightLevel++) {
            int currentLevelStart = startPosition;

            for (int i = 0; i <= heightLevel; i++) {
                pyramid[heightLevel][currentLevelStart] = elements.get(elementsCurrentIndex);
                elementsCurrentIndex++;
                currentLevelStart += 2;
            }

            startPosition--;
        }
    }


}
