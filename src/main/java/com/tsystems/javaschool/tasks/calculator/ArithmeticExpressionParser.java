package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ArithmeticExpressionParser {
    private static final LinkedList<Character> operationTokens = new LinkedList<>((Arrays.asList('(', ')', '+', '-', '*', '/')));

    public static List<Token> parseExpression(String expression) {
        ArrayList<Token> tokens = new ArrayList<>();

        int symbolPosition = 0;
        StringBuilder numberTokens = new StringBuilder();

        while (symbolPosition < expression.length()) {
            char currentSymbol = expression.charAt(symbolPosition);

            if (operationTokens.contains(currentSymbol)) {
                handleNumberTokens(numberTokens, tokens);
                tokens.add(new Token(TokenType.getTokenType(currentSymbol), currentSymbol));
            } else if (currentSymbol >= '0' && currentSymbol <= '9' || currentSymbol == '.') {
                numberTokens.append(currentSymbol);
            } else if (currentSymbol != ' ') {
                throw new RuntimeException("Unexpected character: " + currentSymbol);
            }

            symbolPosition++;
        }

        handleNumberTokens(numberTokens, tokens);

        tokens.add(new Token(TokenType.EOF, ""));
        return tokens;
    }

    /**
     * <p>Method adds number in numberTokens to tokens list</p>
     *
     * @param numberTokens tokens referring to current number
     * @param tokens       current expression tokens
     */
    private static void handleNumberTokens(StringBuilder numberTokens, ArrayList<Token> tokens) {
        if (numberTokens.length() > 0) {
            tokens.add(new Token(TokenType.NUMBER, numberTokens.toString()));
            numberTokens.setLength(0);
        }
    }
}