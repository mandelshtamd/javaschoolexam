package com.tsystems.javaschool.tasks.calculator;

import java.util.List;

public class TokenBuffer {
    private int position;
    private List<Token> tokens;

    public TokenBuffer(List<Token> tokens) {
        this.tokens = tokens;
    }

    public Token next() {
        return tokens.get(position++);
    }

    public boolean hasNext() {
        return position < tokens.size() - 1;
    }

    public void back() {
        position--;
    }
}