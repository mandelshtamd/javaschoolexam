package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }

        try {
            List<Token> tokens = ArithmeticExpressionParser.parseExpression(statement);
            TokenBuffer tokenBuffer = new TokenBuffer(tokens);
            double evaluationResult = ArithmeticTokensEvaluator.evaluate(tokenBuffer);

            if (Double.isInfinite(evaluationResult)) {
                return null;
            }

            DecimalFormat df = new DecimalFormat("#.####", new DecimalFormatSymbols(Locale.US));
            df.setRoundingMode(RoundingMode.CEILING);
            return df.format(evaluationResult);
        } catch (RuntimeException e) {
            return null;
        }
    }

}
