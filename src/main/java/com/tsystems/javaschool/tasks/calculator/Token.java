package com.tsystems.javaschool.tasks.calculator;

enum TokenType {
    LEFT_BRACKET, RIGHT_BRACKET,
    OP_PLUS, OP_MINUS, OP_MUL, OP_DIV,
    NUMBER,
    EOF;

    public static TokenType getTokenType(char c) {
        switch (c) {
            case ('('):
                return LEFT_BRACKET;
            case (')'):
                return RIGHT_BRACKET;
            case ('+'):
                return OP_PLUS;
            case ('-'):
                return OP_MINUS;
            case ('*'):
                return OP_MUL;
            case ('/'):
                return OP_DIV;
            default:
                return NUMBER;
        }
    }
}

public class Token {
    TokenType type;
    String value;

    public Token(TokenType type, String value) {
        this.type = type;
        this.value = value;
    }

    public Token(TokenType type, Character value) {
        this.type = type;
        this.value = value.toString();
    }
}

