package com.tsystems.javaschool.tasks.calculator;

public class ArithmeticTokensEvaluator {

    public static double evaluate(TokenBuffer tokens) {
        Token token = tokens.next();

        if (token.type == TokenType.EOF)
            return 0;
        else {
            tokens.back();
            return handleAdditiveOperations(tokens);
        }
    }

    private static double handleAdditiveOperations(TokenBuffer tokens) {
        double value = handleMultiplicativeOperations(tokens);

        while (tokens.hasNext()) {
            Token token = tokens.next();

            switch (token.type) {
                case OP_PLUS:
                    value += handleMultiplicativeOperations(tokens);
                    break;
                case OP_MINUS:
                    value -= handleMultiplicativeOperations(tokens);
                    break;
                default:
                    tokens.back();
                    return value;
            }
        }
        return value;
    }

    private static double handleMultiplicativeOperations(TokenBuffer tokens) {
        double value = handleOperand(tokens);

        while (tokens.hasNext()) {
            Token token = tokens.next();

            switch (token.type) {
                case OP_MUL:
                    value = value * handleOperand(tokens);
                    break;
                case OP_DIV:
                    value = value / handleOperand(tokens);
                    break;
                default:
                    tokens.back();
                    return value;
            }
        }
        return value;
    }

    private static double handleOperand(TokenBuffer tokens) {
        Token token = tokens.next();

        switch (token.type) {
            case LEFT_BRACKET:
                double value = evaluate(tokens);
                token = tokens.next();

                if (token.type != TokenType.RIGHT_BRACKET) {
                    throw new RuntimeException(String.format("Unexpected token: %s", token.value));
                }
                return value;
            case NUMBER:
                return Double.parseDouble(token.value);
            default:
                throw new RuntimeException(String.format("Unexpected token: %s", token.value));
        }
    }
}
